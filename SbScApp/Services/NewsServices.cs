﻿using Refit;
using SbScApp.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SbScApp.Services
{
   public class NewsServices
    {
        public static async Task<News> GetNews(string source, string apikey)
        {
            News news = new News();

            try
            {
                var client = RestService.For<Interfaces.INews>("https://newsapi.org");
                news = await client.GetNews(source, apikey);
            }
            catch (ApiException ax)
            {

            }
            catch (Exception ex)
            {

            }

            return news;
        }

        public static async Task<SourceR> GetSources(string apiKey)
        {
            SourceR sources = new SourceR();

            try
            {
                var client = RestService.For<Interfaces.INews>("https://newsapi.org");
                sources = await client.GetSources(apiKey);
            }
            catch (ApiException ax)
            {

            }
            catch (Exception ex)
            {

            }

            return sources;
        }
    }
}
