﻿using SbScApp.Model;
using SbScApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SbScApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewsPage : ContentPage
    {
        NewsViewModel Vm => BindingContext as NewsViewModel;
        public NewsPage(Source source)
        {
            InitializeComponent();
            BindingContext = new NewsViewModel(source);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Vm.LoadNews.Execute(null);
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            var item= e.SelectedItem as Article;

            Navigation.PushAsync(new DetailsPage(item));
        }
    }
}