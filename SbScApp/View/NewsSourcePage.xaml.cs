﻿using SbScApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SbScApp.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewsSourcePage : ContentPage
    {
        NewsSourceVm Vm => BindingContext as NewsSourceVm;
        public NewsSourcePage()
        {
            InitializeComponent();
            BindingContext = new NewsSourceVm();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Vm.LoadNewsSource.Execute(null);
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }

            var item = e.SelectedItem as Model.Source;

            Navigation.PushAsync(new View.NewsPage(item));

        }
    }
}