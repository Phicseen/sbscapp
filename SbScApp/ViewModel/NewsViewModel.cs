﻿using SbScApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SbScApp.ViewModel
{
  public  class NewsViewModel:BaseViewModel
    {
        public ICollection<Article> _NewsList;
        public ICollection<Article> NewsList
        {
            get
            {
                return _NewsList;
            }
            set
            {
                SetProperty(ref _NewsList, value);
            }
        }

        
        public string ApiKey = "a06882b3b0714b11bd835c0571c8547a";
        Source source;
        public NewsViewModel(Source source)
        {
            this.source = source;
            Title = source.Name;
        }
        public ICommand LoadNews
        {
            get
            {
                return new Command(async() =>
                {
                    IsBusy = true;
                    var GetNews = await Services.NewsServices.GetNews(source.Id, ApiKey);
                    NewsList = new ObservableCollection<Article>();
                    foreach (var item in GetNews.Articles)
                    {
                        NewsList.Add(item);
                        
                    }
                    IsBusy = false;
                });
            }
        }
    }
}
