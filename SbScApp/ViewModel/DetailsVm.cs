﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SbScApp.ViewModel
{
   public class DetailsVm:BaseViewModel
    {
        Model.Article _Article;
        public Model.Article Article
        {
            get
            {
                return _Article;
            }

            set
            {
                SetProperty(ref _Article, value);
            }
        }


        public DetailsVm(Model.Article article)
        {
            this.Article = article;
        }
    }
}
