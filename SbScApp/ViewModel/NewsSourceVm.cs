﻿using SbScApp.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SbScApp.ViewModel
{
  public  class NewsSourceVm:BaseViewModel
    {
       // public ObservableCollection<Source> Sources { get; set; }

        private ICollection<Source> _Sources;
        public ICollection<Source> Sources
        {
            get
            {
                return _Sources;
            }

            set
            {
                SetProperty(ref _Sources, value);
            }
        }
        public string ApiKey = "a06882b3b0714b11bd835c0571c8547a";
        public ICommand LoadNewsSource
        {
            get
            {
                return new Command(async() =>
                {
                    try
                    {
                        IsBusy = true;
                        var SourceNews = await Services.NewsServices.GetSources(ApiKey);
                        Sources = new ObservableCollection<Source>();
                        foreach (var item in SourceNews.Sources)
                        {
                          
                            Sources.Add(item);
                        }
                        //Sources = new ObservableCollection<Source>(SourceNews.Sources);
                        IsBusy = false;
                      }
                    catch (Exception ex)
                    {

                      
                    }
                   
                });
            }
        }
    }
}
