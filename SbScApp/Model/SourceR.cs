﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SbScApp.Model
{
   public class SourceR
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("sources")]
        public List<Source> Sources { get; set; }
    }
}
