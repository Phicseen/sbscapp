﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SbScApp.Model
{
   public class News
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("articles")]
        public IList<Article> Articles { get; set; }
    }
}
