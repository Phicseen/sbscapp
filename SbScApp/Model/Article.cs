﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SbScApp.Model
{
   public class Article
    {
        [JsonProperty("source")]
        public Source Source { get; set; }
        [JsonProperty("author")]
        public string Author { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("urlToImage")]
        public string UrlToImage { get; set; }
        [JsonProperty("publishedAt")]
        public DateTime? PublishedAt { get; set; }
        [JsonIgnore]
        public string PublishedAtText { get; set; }
    }
}
