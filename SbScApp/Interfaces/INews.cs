﻿using Refit;
using SbScApp.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SbScApp.Interfaces
{
    public interface INews
    {
        [Get("/v2/top-headlines?sources={source}&apiKey={apikey}")]
        Task<News> GetNews(string source, string apikey);

        [Get("/v2/sources?apiKey={apikey}")]
        Task<SourceR> GetSources(string apikey);
    }
}
